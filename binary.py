#!/usr/bin/env python3
# Binary
# Copyright © 2019 Mark Damon Hughes
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following adisclaimer in the
# documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os, sys, re

def encodeBinary(text):
	outText = ""
	for i in range(len(text)):
		c = text[i]
		s = bin(ord(c))[2:]+" "
		if len(s) < 8:
			s = ("00000000"+s)[-8:]
		outText += s
	return outText

def decodeBinary(text):
	outText = ""
	words = re.split(r"([^01]+)", text)
	for w in words:
		if re.fullmatch(r"[01]+", w):
			try:
				bits = int(w, base=2)
				outText += chr(bits)
			except ValueError as e:
				outText += w
		else:
			outText += w
	return outText

def usage():
	raise Exception("Usage: binary.py [-e|-encode|-d|-decode] [FILENAME]")

def main(argv):
	filename = None
	encode = True
	i = 0
	while i < len(argv):
		a = argv[i]
		i += 1
		if a.startswith("-"):
			if a == "-e" or a == "-encode":
				encode = True
			elif a == "-d" or a == "-decode":
				encode = False
			else:
				usage()
		elif not filename:
			filename = a
		else:
			usage()

	if filename:
		fd = open(filename, "r")
		text = fd.read()
		fd.close()
	else:
		text = sys.stdin.read()

	if encode:
		print(encodeBinary(text), end='')
	else:
		print(decodeBinary(text), end='')

if __name__ == "__main__":
	main(sys.argv[1:])
